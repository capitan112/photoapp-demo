#import "ArhiveController.h"

@interface ArhiveController ()

@end

@implementation ArhiveController

- (id)init {
    self = [super init];
    if (self) {
        backgroundImage = [UIImage imageNamed:@"violetNavBar"];
        [navigBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        navLabel.text = @"Arhive";
        imgView.image = nil;
        [self createLabel];
        [self getPhoto];
        library = [ArhiveController defaultAssetsLibrary];
        twitterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [twitterBtn setBackgroundImage:[UIImage imageNamed:@"twitterBtn"] forState:UIControlStateNormal];
        [twitterBtn addTarget:self action:@selector(twitterBtnTap:) forControlEvents:UIControlEventTouchUpInside];
        facebookBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [facebookBtn setBackgroundImage:[UIImage imageNamed:@"facebookBtn"] forState:UIControlStateNormal];
        [facebookBtn addTarget:self action:@selector(facebookBtnTap:) forControlEvents:UIControlEventTouchUpInside];
        if (IS_IPHONE_5) {
            twitterBtn.frame   = CGRectMake(24, SOCIALBUTTONTOPMARGIN + 90,  35,  35);
            facebookBtn.frame   = CGRectMake(261, SOCIALBUTTONTOPMARGIN + 90,  35,  35);
        } else {
            twitterBtn.frame   = CGRectMake(24, SOCIALBUTTONTOPMARGIN,  35,  35);
            facebookBtn.frame   = CGRectMake(261, SOCIALBUTTONTOPMARGIN,  35,  35);
        }
        [self.view addSubview:twitterBtn];
        [self.view addSubview:facebookBtn];
        [twitterBtn bringSubviewToFront:self.view];
        [facebookBtn bringSubviewToFront:self.view];
        currentIndex = 0;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getPhoto];
}

- (void)viewWillAppear:(BOOL)animated {
    [self getPhoto];
    currentIndex = 0;
    [self delayWithSecs:0.8];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [self getPhoto];
    carousel = [[iCarousel alloc] initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.width)];
    carousel.delegate = self;
    carousel.dataSource = self;
    [self.view addSubview:carousel];
    carousel.type = iCarouselTypeCoverFlow2;
    [carousel scrollToItemAtIndex:5 animated:NO];
}

- (void)viewDidDisappear:(BOOL)animated{
    [carousel removeFromSuperview];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

#pragma mark Create Content

- (void) createButton {
}

- (void) createLabel {
    dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 32)];
    dateLabel.center = CGPointMake(self.centerPoint.x, self.centerPoint.y + 310);
    dateLabel.textColor = [UIColor colorWithRed:90/255.0f green:91/255.0f blue:77/255.0f alpha:1.0f];
    dateLabel.text = @"June 3";
    dateLabel.textAlignment = NSTextAlignmentCenter;
    dateLabel.font = [UIFont fontWithName:@"Superclarendon-Light" size:16];
    dateLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:dateLabel];
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMMM d"];
}

#pragma mark Button action

- (IBAction)twitterBtnTap:(id)sender {
//     NSLog(@"twitter button tapped");
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *twitterDialog = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];

        [twitterDialog setInitialText:@" "];
        [twitterDialog addImage:[self getImageByIndex:currentIndex].image];
        [self presentViewController:twitterDialog animated:YES completion:nil];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                            message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (IBAction)facebookBtnTap:(id)sender {
//    NSLog(@"facebook button tapped");
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *facebookDialog = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [facebookDialog setInitialText:@" "];
        [facebookDialog addImage:[self getImageByIndex:currentIndex].image];
        [self presentViewController:facebookDialog animated:YES completion:nil];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                            message:@"You can't send a facebook post right now, make sure your device has an internet connection and you have at least one Facebook account setup"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (UIImageView *)getImageByIndex:(NSUInteger)index {
    ALAsset *asset = [photos objectAtIndex:index];
    UIImageView *indexView = [[UIImageView new] initWithImage:[UIImage imageWithCGImage:[asset thumbnail]]];
    return indexView;
}

#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    return [photos count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view {
    ALAsset *asset = [photos objectAtIndex:index];
    currentIndex = index;
    view = [[UIImageView new] initWithImage:[UIImage imageWithCGImage:[asset thumbnail]]];
    view.frame = CGRectMake(0, 0, self.view.frame.size.width - 60, self.view.frame.size.width - 60);
    NSDate *date = [asset valueForProperty:ALAssetPropertyDate];
    NSString *date_string = [dateFormat stringFromDate:date];
    dateLabel.text = date_string;
    [dateLabel setNeedsDisplay];
    
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value {
    switch (option) {
        case iCarouselOptionWrap: {
            return YES;
        }
        default: {
            return value;
        }
    }
}

@end
