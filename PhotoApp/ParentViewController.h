#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define SOCIALBUTTONTOPMARGIN 375

@interface ParentViewController : UIViewController {
    UINavigationBar *navigBar;
    UIImage *backgroundImage;
    UIImageView *imgView;
    UILabel *navLabel;
    UIButton *button;
    CGRect croppedRect;
    int radius;
    ALAssetsLibrary *library;
    NSMutableArray *photos;
}

@property (nonatomic, assign) CGPoint centerPoint;

+ (ALAssetsLibrary *)defaultAssetsLibrary;

- (void) createContent;
- (void) createButton;
- (void)delayWithSecs:(NSTimeInterval)secs;
- (void)getPhoto;

@end
