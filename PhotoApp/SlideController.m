#import "SlideController.h"

@interface SlideController ()

@end

@implementation SlideController

- (id)init{
    self = [super init];
    if (self) {
        backgroundImage = [UIImage imageNamed:@"orangeNavBar"];
        [navigBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        navLabel.text = @"Show";
        imgView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        imgView.image = nil;
        imageLayer = [CALayer layer];
        imageLayer.frame = CGRectMake(30, 70, self.view.frame.size.width - 60, self.view.frame.size.width - 60);
        imageLayer.contents = (__bridge id)([UIImage imageNamed:@"girl"].CGImage);
        imageLayer.backgroundColor = [UIColor clearColor].CGColor;
        [imgView.layer addSublayer:imageLayer];
        button.center = CGPointMake(self.centerPoint.x, self.centerPoint.y + 150);
        [button setImage:[UIImage imageNamed:@"arrowShowSlide"] forState:UIControlStateNormal];
        [self createLabel];
        [self createSlider];
        if (IS_IPHONE_5) {
            slider.center = CGPointMake(self.centerPoint.x, self.centerPoint.y + 400);
            numberOfSlidesLabel.center = CGPointMake(self.centerPoint.x, self.centerPoint.y + 320);
        } else {
            slider.center = CGPointMake(self.centerPoint.x, self.centerPoint.y + 345);
            numberOfSlidesLabel.center = CGPointMake(self.centerPoint.x, self.centerPoint.y + 310);
        }
        slides = @" slides";
        slide = @" slide";
        [self getPhoto];
        slideShowGo = NO;
        currentIndex = 0;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getPhoto];
}

- (void) viewDidAppear:(BOOL)animated {
    [self getPhoto];
}

- (void)viewWillAppear:(BOOL)animated {
    [self getPhoto];
    [self delayWithSecs:0.8];
    slider.maximumValue = [photos count] - 1;
    numberOfSlidesLabel.text = [self numberOfSlides:[photos count]];
    currentIndex = 0;
    if (photos == nil) {
        [self updateImageAtIndex:currentIndex];
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    [self stopTimer];
    if (photos == nil) {
        [self updateImageAtIndex:currentIndex];
    }
    slideShowGo = NO;
    slider.value = 0;
    currentIndex = 0;
 
}

#pragma mark - Create content

- (void)createLabel {
    numberOfSlidesLabel                 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 50)];
    numberOfSlidesLabel.textColor       = [UIColor colorWithRed:90/255.0f green:91/255.0f blue:77/255.0f alpha:1.0f];
    numberOfSlidesLabel.textAlignment   = NSTextAlignmentCenter;
    numberOfSlidesLabel.font            = [UIFont fontWithName:@"Superclarendon-Light" size:18];
    numberOfSlidesLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview: numberOfSlidesLabel];
}

-(void)createSlider {
    slider                 = [[UISlider alloc] initWithFrame:CGRectMake(0.0, 0.0, 300.0, 32.0)];
    slider.center          = CGPointMake(self.centerPoint.x, self.centerPoint.y + 345);
    [slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    slider.backgroundColor = [UIColor clearColor];
    [slider setMinimumTrackImage:[UIImage imageNamed:@"violetNavBar"] forState:UIControlStateNormal];
    UIImage *image         = [UIImage imageNamed:@"orangeNavBar"];
    [slider setThumbImage:image forState:UIControlStateNormal];
    UIColor *color         = [UIColor colorWithRed:242/255.0f green:155/255.0f blue:18/255.0f alpha:1.0f];
    [slider setThumbTintColor:color];
    slider.minimumValue    = 0.0;
    slider.maximumValue    = 0.0;
    slider.continuous      = YES;
    slider.value           = currentIndex;
    [self.view addSubview: slider];
    
}

#pragma mark - Button and slider action

- (IBAction) sliderAction:(id)sender {
//    if (photos.count > 0) {
        [self updateImageAtIndex:slider.value];
        currentIndex = slider.value;
//    }
}

- (IBAction)circelButtonPressed:(id)sender {
    if (!slideShowGo) {
        [self startTimer];
        slideShowGo = YES;
     
    } else {
        [self stopTimer];
        slideShowGo = NO;
    }
}

- (void)startTimer {
    if (!timer) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1.5
                                                 target:self
                                               selector:@selector(updatePhoto)
                                               userInfo:nil repeats:YES];
    }
   [button setImage:nil forState:UIControlStateNormal];
}

- (void)stopTimer {
    if ([timer isValid]) {
        [timer invalidate];
    }
    timer = nil;
    currentIndex = 0;
    [button setImage:[UIImage imageNamed:@"arrowShowSlide"] forState:UIControlStateNormal];
}

#pragma mark - Photo function

- (void)updatePhoto {
    if (currentIndex < [photos count] - 1) {
        currentIndex += 1;
        [slider setValue: slider.value + 1];
    } else if (currentIndex == [photos count]  - 1) {
        currentIndex = 0;
        [slider setValue:0];
    }
    [self updateImageAtIndex:currentIndex];
}

- (void)updateImageAtIndex:(NSUInteger)index {
    ALAsset *asset = [photos objectAtIndex:index];
    UIImage *contentImage = [UIImage imageWithCGImage:[asset thumbnail]];
    imageLayer.contents = (__bridge id)(contentImage.CGImage);
}

#pragma mark - Text function

- (NSString *)numberOfSlides:(int)numbersOfPhoto {
    NSString *numberOfPhotoString = [NSString stringWithFormat:@"%d", numbersOfPhoto];
    if ([photos count] > 1) {
        numberOfPhotoString = [numberOfPhotoString stringByAppendingString:slides];
    } else if ([photos count] == 1) {
        numberOfPhotoString = [numberOfPhotoString stringByAppendingString:slide];
    } else if ([photos count] <= 0 ) {
        numberOfPhotoString = @"no slides";
    }
    
    return numberOfPhotoString;
}

@end
