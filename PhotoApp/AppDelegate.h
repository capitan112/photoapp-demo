#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "CameraController.h"
#import "ArhiveController.h"
#import "SlideController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
