#import "ParentViewController.h"

@interface SlideController : ParentViewController {
    NSTimer *timer;
    int currentIndex;
    CALayer *imageLayer;
    UISlider *slider;
    UILabel *numberOfSlidesLabel;
    NSString *slides;
    NSString *slide;
    BOOL slideShowGo;
}

@end
