#import "ParentViewController.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import <ImageIO/ImageIO.h>
#import "ArhiveController.h"


@interface CameraController : ParentViewController {
    UIImageView *vImagePreview;
    UIImageView *readyImage;
    
    AVCaptureSession *session;
    CGRect currentFrame;
    UIAlertView *alert;
    UIImageView *alertImageView;
    CALayer *backgroundLayer;
    CALayer *maskLayer;
@public
    ArhiveController *arhiveViewController;
}

@property(nonatomic, retain) AVCaptureStillImageOutput *stillImageOutput;
@property(nonatomic, retain) UIImageView *vImage;

//+ (ALAssetsLibrary *)defaultAssetsLibrary;

@end
