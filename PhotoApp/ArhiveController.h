#import "ParentViewController.h"
#import "SlideController.h"
#import "iCarousel.h"
#import <Social/Social.h>

@interface ArhiveController : ParentViewController <iCarouselDataSource, iCarouselDelegate> {
    CALayer *importImage;
    CALayer *girlLayer;
    int currentIndex;
    UILabel *dateLabel;
    NSDateFormatter *dateFormat;
    iCarousel *carousel;
    UIImageView *myView;
    UIButton *twitterBtn;
    UIButton *facebookBtn;
@public
    SlideController *slideShowViewController;
}

@end
