#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    CameraController *cameraController = [CameraController new];
    
    ArhiveController *arhiveViewController = [ArhiveController new];
    SlideController *slideShowViewController = [SlideController new];
    cameraController->arhiveViewController = arhiveViewController;
    arhiveViewController->slideShowViewController = slideShowViewController;
    arhiveViewController.view.backgroundColor = [UIColor clearColor];
    slideShowViewController.view.backgroundColor = [UIColor clearColor];
//    UINavigationController *rootNavigationView = [[UINavigationController alloc] initWithRootViewController:cameraController];
//    [rootNavigationView setNavigationBarHidden:YES];
//    [rootNavigationView setToolbarHidden:YES];
    
    UINavigationController *rootNavigationView = [[UINavigationController alloc] initWithRootViewController:arhiveViewController];
    rootNavigationView.view.backgroundColor = [UIColor clearColor];
    [rootNavigationView setNavigationBarHidden:YES];
    [rootNavigationView setToolbarHidden:YES];
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    tabBarController.tabBar.barStyle = UIBarStyleBlack;
    tabBarController.viewControllers = [NSArray arrayWithObjects: cameraController,
                                                                  rootNavigationView,
                                                                  slideShowViewController, nil];
    
    UITabBarItem *item1 = [[UITabBarItem alloc] initWithTitle:@"Camera" image:nil tag:1];
    [cameraController setTabBarItem:item1];
    UITabBarItem *item2 = [[UITabBarItem alloc] initWithTitle:@"Archive" image:nil tag:2];
    [rootNavigationView setTabBarItem:item2];
    UITabBarItem *item3 = [[UITabBarItem alloc] initWithTitle:@"SlideShow" image:nil tag:3];
    [slideShowViewController setTabBarItem:item3];

    self.window.rootViewController = tabBarController;
    
//    self.window.rootViewController = rootNavigationView;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    return UIInterfaceOrientationMaskPortrait;
}



@end
