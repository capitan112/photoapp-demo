#import "CameraController.h"

@interface CameraController ()

@end

@implementation CameraController

- (id)init {
    self = [super init];
    if (self) {
        _stillImageOutput = [AVCaptureStillImageOutput new];
        vImagePreview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,
                                                                      self.view.frame.size.height)];
        
        backgroundLayer = [CALayer layer];
        backgroundLayer.frame = CGRectMake(0, 0, self.view.frame.size.width,
                                           self.view.frame.size.height);
        backgroundLayer.backgroundColor = [UIColor clearColor].CGColor;
        backgroundLayer.position = self.view.center;
        backgroundLayer.zPosition = 1;
        [vImagePreview.layer addSublayer:backgroundLayer];
        maskLayer = [CALayer layer];
        maskLayer.frame = imgView.frame;
        maskLayer.position = imgView.center;
        [vImagePreview.layer addSublayer:maskLayer];
        maskLayer.contents = (__bridge id)imgView.image.CGImage;
        maskLayer.cornerRadius = roundf(maskLayer.bounds.size.width/2.0);
        maskLayer.masksToBounds = YES;
        vImagePreview.layer.zPosition = 2;
        [self.view addSubview:vImagePreview];
        library = [CameraController defaultAssetsLibrary];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated {
  [self delayWithSecs:0.7];
}

- (void) viewDidAppear:(BOOL)animated {
	session = [AVCaptureSession new];
    _stillImageOutput = [AVCaptureStillImageOutput new];
	session.sessionPreset = AVCaptureSessionPresetMedium;
	AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    captureVideoPreviewLayer.frame = backgroundLayer.bounds;
    [backgroundLayer addSublayer:captureVideoPreviewLayer];
    backgroundLayer.mask = maskLayer;
    AVCaptureDevice *device = [self frontCamera];
	NSError *error = nil;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
	[session addInput:input];
	[session startRunning];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [_stillImageOutput setOutputSettings:outputSettings];
    [session addOutput:_stillImageOutput];
}

#pragma mark Camera's at device

- (AVCaptureDevice *)frontCamera {
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == AVCaptureDevicePositionFront) {
            return device;
        }
    }
    return nil;
}

#pragma mark Button capture

- (IBAction)circelButtonPressed:(id)sender {
	AVCaptureConnection *videoConnection = nil;
	for (AVCaptureConnection *connection in _stillImageOutput.connections) {
		for (AVCaptureInputPort *port in [connection inputPorts]) {
			if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
				videoConnection = connection;
				break;
			}
		}
		if (videoConnection) {
            break;
        }
	}
	[_stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
        NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
        UIImage *image = [[UIImage alloc] initWithData:imageData];
        UIImageView *overlap = [UIImageView new];
        overlap.frame = CGRectMake(0, 0, image.size.width, image.size.height);
        overlap.center = imgView.center;
        overlap.image = image;
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, image.size.width, image.size.height) cornerRadius:0];
        UIBezierPath *circlePath = [UIBezierPath bezierPathWithRoundedRect:croppedRect cornerRadius:radius];
        [path appendPath:circlePath];
        [path setUsesEvenOddFillRule:YES];
        CAShapeLayer *fillLayer = [CAShapeLayer layer];
        fillLayer.path = path.CGPath;
        fillLayer.fillRule = kCAFillRuleEvenOdd;
        fillLayer.fillColor = [UIColor whiteColor].CGColor;
        fillLayer.opacity = 1;
        [overlap.layer addSublayer:fillLayer];
        UIImage *resultImage = [self cropImage:[self getUIImageFromThisUIView:overlap] inRect:croppedRect];
        library = [CameraController  defaultAssetsLibrary];
        [library writeImageToSavedPhotosAlbum:[resultImage CGImage]
                                                   orientation:ALAssetOrientationUp
                                               completionBlock:^(NSURL *assetURL, NSError *error) {
           if(assetURL) {
                [library assetForURL:assetURL resultBlock:^(ALAsset *asset) {
                    [self addAsset:asset toGroup:@"Selfies" inLib:library];
                } failureBlock:^(NSError *error) {
                }];
            }
            
        }];
    }];
    button.enabled = NO;
    [self delayWithSecs:0.7];
    button.enabled = YES;
}

- (void) addAsset:(ALAsset*)a toGroup:(NSString*)name inLib:(ALAssetsLibrary*)lib {
    [lib enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if([[group valueForProperty:ALAssetsGroupPropertyName] isEqualToString:name]) {
            [group addAsset:a];
            *stop = YES;
        }
        if(!group) {
            [lib addAssetsGroupAlbumWithName:name resultBlock:^(ALAssetsGroup *group) {
                [group addAsset:a];
            } failureBlock:^(NSError *error) {
                NSLog(@"e: %@", error);
            }];
        }
    } failureBlock:^(NSError *error) { }];
}
#pragma mark - Image and alert function

- (UIImage*)getUIImageFromThisUIView:(UIView*)aUIView {
    UIGraphicsBeginImageContext(aUIView.bounds.size);
    [aUIView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return viewImage;
}

- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize; {
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)cropImage:(UIImage *)image inRect:(CGRect)rect {
    CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, rect);
    UIImage *resultImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return resultImage;
}

@end
