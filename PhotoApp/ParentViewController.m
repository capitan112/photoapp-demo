#import "ParentViewController.h"
#define NAVIGBARHEIGHT 60

@interface ParentViewController ()

@end

@implementation ParentViewController

+ (ALAssetsLibrary *)defaultAssetsLibrary {
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    return library;
}

- (id)init {
    self = [super init];
    if (self) {
        [self createContent];
        [self createButton];
        library = [ParentViewController defaultAssetsLibrary];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _centerPoint = CGPointMake(self.view.frame.size.width / 2, 50);
    radius = 150;
    croppedRect = CGRectMake(30, 65, 2.0*radius, 2.0*radius);
}

#pragma mark - Create content

- (void) createContent {
    navigBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, NAVIGBARHEIGHT)];
    backgroundImage = [UIImage imageNamed:@"greenNavBar"];
    [navigBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 32)];
    navLabel.center = CGPointMake(_centerPoint.x, _centerPoint.y - 10);
    navLabel.font = [UIFont fontWithName:@"Superclarendon-Light" size:18];
    navLabel.text = @"Camera";
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.textColor = [UIColor whiteColor];
    navLabel.backgroundColor = [UIColor clearColor];
    [navigBar addSubview:navLabel];
    [self.view addSubview:navigBar];
    imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 30, self.view.frame.size.width - 30)];
    imgView.center = CGPointMake(_centerPoint.x, _centerPoint.y + 160);
    imgView.image = [UIImage imageNamed:@"girl"];
    [self.view addSubview:imgView];
    photos = [NSMutableArray new];
}

- (void) createButton {
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"greenButton"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(circelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"" forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 70.0, 70.0);
    if (IS_IPHONE_5) {
        button.center = CGPointMake(_centerPoint.x, _centerPoint.y + 400);
    } else {
        button.center = CGPointMake(_centerPoint.x, _centerPoint.y + 345);
    }
    [self.view addSubview:button];
}

#pragma mark - Buton action

- (IBAction)circelButtonPressed:(id)sender {
    NSLog(@"Button pressed");
}

#pragma mark - Delay

- (void)delayWithSecs:(NSTimeInterval)secs {
    [NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:secs]];
}


#pragma mark - Photo

- (void)getPhoto {
    NSMutableArray *collector = [[NSMutableArray alloc] initWithCapacity:0];
    library = [ParentViewController defaultAssetsLibrary];
    [library enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock: ^(ALAssetsGroup *group, BOOL *stop) {
        [group setAssetsFilter:[ALAssetsFilter allPhotos]];
        if([[group valueForProperty:ALAssetsGroupPropertyName] isEqualToString:@"Selfies"]) {
            if (group == nil) {
                return;
            } else if (group != nil) {
                [group enumerateAssetsUsingBlock:^(ALAsset *asset, NSUInteger index, BOOL *stop) {
                    if (asset) {
                        [collector addObject:asset];
                    }
                }];
                photos = collector;
            }
        }
    }
                         failureBlock:^(NSError *error) {
                         } ];
}


@end
